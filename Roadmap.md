## Next major release

To Be Decided (TBD). We had a recent release. [Check them out](https://github.com/dotnet-architecture/eShopOnContainers/wiki/Release-notes)

Check the [backlog](Backlog) for candidate features.
